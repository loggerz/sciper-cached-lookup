# Sciper Cached Lookup

Simple Rust project to convert csv format with data from [Search@EPFL](https://search.epfl.ch).

## Install

Install Rust (edition 2021 or later), run `cargo r` to install the dependencies and display the help menu.

To install on the local set of installed binary crates : `cargo install --path .`

## How to use

NB: This is how to use with the CLI but the API is also available as Rust library.

### Searching with only one input

`sciper-cached-lookup sciper 324427` or `sciper-cached-lookup unit agepinfo`

### Searching with a list of scipers

Have a file like tests/scipers.txt of a sciper to try on each line.

Then you can use for examples:
```
# To preview what data you are producing
cat tests/scipers.txt | sciper-cached-lookup scipers | less

# To export to a file
cat tests/scipers.txt | sciper-cached-lookup > tests/emails.txt

# To export to a file only people's email
cat tests/scipers.txt | sciper-cached-lookup -o email scipers > tests/emails.txt
```

### Cache management

Each query to an unknown Sciper will trigger a online search on search.epfl.ch to find the profile and the associated units.
People in the associated units will also be downloaded to anticipate other probable results.

```
# To see what is currently stored in the cache
sciper-cached-lookup cache-dump

# To specify where the cache file should be stored
sciper-cached-lookup -c ~/.my-hidden-cache.json [CMD] [ARGs..]
```

## To-do

- [ ] Searching with a list of units
- [ ] Unix piping output (currently buggy)
- [ ] Option to overwrite cache
use std::collections::HashMap;
use std::path::PathBuf;
use serde::{Serialize, Deserialize};
use crate::{LookupError, Person, sciper_lookup, Unit, unit_lookup};

#[derive(Debug)]
pub enum CacheError {
    CacheCorrupted,
    PermissionDenied,
    UnitRecursionError,
}

impl ToString for CacheError {
    fn to_string(&self) -> String {
        match self {
            CacheError::CacheCorrupted => String::from("The cache is corrupted, cannot proceed without losing data."),
            CacheError::PermissionDenied => String::from("Can't access cache file."),
            CacheError::UnitRecursionError => String::from("EPFL API returned incoherent data (bad relations between objects)."),
        }
    }
}

#[derive(Deserialize, Serialize)]
pub struct Cache {
    pub units: HashMap<String, Unit>,
    pub people: HashMap<String, Person>,
}

impl Cache {
    pub fn load(cache_path: &PathBuf) -> Result<Cache, CacheError> {
        if !cache_path.exists() {
            return Ok(Cache { units: HashMap::new(), people: HashMap::new() })
        }

        let file = std::fs::File::open(cache_path).map_err(|_| CacheError::PermissionDenied)?;
        let reader= std::io::BufReader::new(file);
        let cache: Cache = serde_json::from_reader(reader).map_err(|_| CacheError::CacheCorrupted)?;
        Ok(cache)
    }

    pub fn save(&self, cache_path: &PathBuf) -> Result<(), CacheError> {
        let file = std::fs::File::create(cache_path).map_err(|_| CacheError::PermissionDenied)?;
        let writer = std::io::BufWriter::new(file);
        serde_json::to_writer(writer, &self).map_err(|_| CacheError::PermissionDenied)?;
        Ok(())
    }

    pub async fn sciper_lookup(&mut self, sciper: &String) -> Result<Person, LookupError> {
        match self.people.get(sciper) {
            Some(p) => Ok(p.clone()),
            None => {
                let p = sciper_lookup(sciper).await?;
                self.people.insert(sciper.clone(), p.clone());
                if let Some(accreds) = &p.accreds {
                    for accred in accreds {
                        match unit_lookup(&accred.acronym).await {
                            Ok(unit) => {
                                if let Some(people) = unit.clone().people {
                                    for p2 in people {
                                        self.people.insert(p2.clone().sciper, p2);
                                    }
                                }
                                self.units.insert(unit.acronym.clone(), unit);
                            },
                            Err(err) => {
                                eprintln!("Looking up {}", &accred.acronym);
                                eprintln!("Error: {:?}", err);
                                return Err(LookupError::CacheError(CacheError::UnitRecursionError));
                            }
                        }
                    }
                }
                Ok(p)
            }
        }
    }

    pub async fn unit_lookup(&self, acronym: &String) -> Result<Unit, LookupError> {
        match self.units.get(acronym) {
            Some(u) => Ok(u.clone()),
            None => unit_lookup(acronym).await
        }
    }
}
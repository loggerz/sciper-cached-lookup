use regex::Regex;
use serde::{Serialize, Deserialize};

use cache::CacheError;

pub mod cache;

#[derive(Debug)]
pub enum LookupError {
    SciperInvalid,
    MultipleResults,
    NotFound,
    InvalidInput,
    NetworkError,
    ApiFormatError,
    CacheError(CacheError),
}

impl LookupError {
    pub fn print(&self) {
        match self {
            LookupError::NetworkError => eprintln!("A network error occurred."),
            LookupError::SciperInvalid => eprintln!(r"The Sciper number you entered is invalid. Scipers must follows ^[0-9]{{6}}$."),
            LookupError::InvalidInput => eprintln!("Unable to parse input format."),
            LookupError::NotFound => eprintln!("No record found for the given input."),
            LookupError::ApiFormatError => eprintln!("The EPFL API responded in an unexpected format."),
            LookupError::MultipleResults => eprintln!("Multiple results were returned where only one was expected."),
            LookupError::CacheError(err) => eprintln!("Cache error: {}", err.to_string()),
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Person {
    pub name: String,
    pub firstname: String,
    pub email: Option<String>,
    pub sciper: String,
    pub profile: String,
    pub accreds: Option<Vec<Unit>>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Unit {
    pub acronym: String,
    pub name: String,
    #[serde(rename = "unitPath")]
    pub unit_path: Option<String>,
    pub people: Option<Vec<Person>>,
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum UnitOrVec {
    U(Unit),
    V(Vec<Unit>),
}

pub async fn sciper_lookup(sciper: &String) -> Result<Person, LookupError> {
    let sciper_regex: Regex = Regex::new(r"^[0-9]{6}$").unwrap();
    if !sciper_regex.is_match(&sciper) { return Err(LookupError::SciperInvalid); }

    let url = format!("https://search-api.epfl.ch/api/ldap?showall=0&hl=en&pageSize=all&siteSearch=people.epfl.ch&q={}", sciper);
    let res = reqwest::get(url).await.map_err(|_| LookupError::NetworkError)?;
    let mut profiles: Vec<Person> = res.json().await.map_err(|_| LookupError::ApiFormatError)?;

    if profiles.len() == 0 { return Err(LookupError::NotFound) }
    else if profiles.len() > 1 { return Err(LookupError::MultipleResults) }

    Ok(profiles.pop().unwrap())
}

pub async fn unit_lookup(acronym: &String) -> Result<Unit, LookupError> {
    // let url = format!("https://search-api.epfl.ch/api/unit/json?q={}", acronym);
    let url = format!("https://search-api.epfl.ch/api/unit?hl=en&showall=0&siteSearch=unit.epfl.ch&acro={}", acronym);
    let res = reqwest::get(url).await.map_err(|_| LookupError::NetworkError)?;

    if res.content_length().unwrap_or(0) == 2 {
        return Err(LookupError::NotFound);
    }

    let unit_or_vec: UnitOrVec = res.json().await.map_err(|_| LookupError::ApiFormatError)?;
    match unit_or_vec {
        UnitOrVec::U(unit) => {
            Ok(unit)
        },
        UnitOrVec::V(_units) => {
            Err(LookupError::MultipleResults)
        }
    }
}
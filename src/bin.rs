use std::process::exit;
use serde::Serialize;
use std::path::PathBuf;
use clap::{Parser, Subcommand, ValueEnum};

use sciper_cached_lookup::*;
use sciper_cached_lookup::cache::Cache;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// Overwrite the default cache file
    #[arg(short, long, default_value = "/tmp/sciper-cached-lookup.json")]
    cache: PathBuf,

    /// Format to print output
    #[arg(short, long, default_value = "human")]
    output: OutputFormats,

    #[command(subcommand)]
    command: MainCommand,
}

#[derive(Clone, PartialEq, ValueEnum, Serialize)]
enum OutputFormats {
    Human,
    CSV,
    JSON,
    Email,
}

impl ToString for OutputFormats {
    fn to_string(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }
}

#[derive(Subcommand, Debug)]
enum MainCommand {
    /// Single sciper lookup
    Sciper {
        /// sciper of the person to lookup
        sciper: String,
    },

    /// Multiple scipers via stdin
    Scipers {},

    /// Single unit lookup
    Unit {
        acronym: String,
    },

    /// Dump the content of the cache
    CacheDump {}
}

#[tokio::main]
async fn main() {
    let cli = Cli::parse();

    if cli.output == OutputFormats::JSON {
        eprintln!("Format unimplemented !");
        exit(1);
    }

    match cli.command {
        MainCommand::Sciper { sciper } => {
            let mut cache = match Cache::load(&cli.cache) {
                Ok(c) => c,
                Err(err) => {
                    eprintln!("Error while loading cache: {:?}", err);
                    exit(1);
                }
            };

            let lookup = cache.sciper_lookup(&sciper).await;
            match lookup {
                Ok(p) => {
                    println!("Sciper: {}", p.sciper);
                    println!("Firstname: {}", p.firstname);
                    println!("Name: {}", p.name);
                    println!("Email: {}", p.email.unwrap_or("null".to_string()));
                    if let Some(accreds) = p.accreds {
                        println!("Accreds: ");
                        for unit in accreds {
                            println!(" >> {} - {}", unit.acronym, unit.name)
                        }
                    } else {
                        println!("Accreds: nothing");
                    }
                    if let Err(err) = cache.save(&cli.cache) {
                        eprintln!("Error while saving cache: {:?}", err);
                        exit(1);
                    }
                },
                Err(err) => {
                    err.print();
                    exit(1);
                }
            }
        },

        MainCommand::Scipers {} => {
            fn batch_failed(_: std::io::Error) -> String {
                LookupError::InvalidInput.print();
                exit(1);
            }

            let stdin = std::io::stdin();
            let lines: Vec<String> = stdin.lines().map(|l| l.unwrap_or_else(batch_failed)).collect();

            if cli.output == OutputFormats::CSV {
                println!("sciper,firstname,name,email")
            }

            let mut cache = match Cache::load(&cli.cache) {
                Ok(c) => c,
                Err(err) => {
                    eprintln!("Error while loading cache: {:?}", err);
                    exit(1);
                }
            };

            for (i,line) in lines.iter().enumerate() {
                match cache.sciper_lookup(&line).await {
                    Ok(p) => {
                        if cli.output == OutputFormats::Email {
                            println!("{}", p.email.unwrap_or("null".to_string()));
                        } else if cli.output == OutputFormats::CSV {
                            println!("{},{},{},{}", p.sciper, p.firstname, p.name, p.email.unwrap_or("null".to_string()));
                        } else {
                            println!("---- #{} -- {} ----", i, p.sciper);
                            println!("Firstname: {}", p.firstname);
                            println!("Name: {}", p.name);
                            println!("Email: {}", p.email.unwrap_or("null".to_string()));
                            println!("---- #{}# ----", i);
                        }
                    },
                    Err(err) => {
                        println!("null");
                        err.print()
                    }
                }
            }

            if let Err(err) = cache.save(&cli.cache) {
                eprintln!("Error while saving cache: {:?}", err);
                exit(1);
            }
        },

        MainCommand::Unit {acronym} => {
            let cache = match Cache::load(&cli.cache) {
                Ok(c) => c,
                Err(err) => {
                    eprintln!("Error while loading cache: {:?}", err);
                    exit(1);
                }
            };
            let lookup = cache.unit_lookup(&acronym).await;
            match lookup {
                Ok(p) => {
                    println!("Unit path: {}", p.unit_path.unwrap_or("null".to_string()));
                    println!("Name: {}", p.name);

                    if let Some(people) = p.people {
                        println!("People: ");
                        for person in people {
                            println!(" >> {} - {} {}", person.sciper, person.firstname, person.name);
                        }
                    } else {
                        println!("People: nobody")
                    }

                },
                Err(err) => {
                    err.print();
                    exit(1);
                }
            }
        },

        MainCommand::CacheDump {} => {
            match Cache::load(&cli.cache) {
                Ok(cache) => {
                    println!(">------- START OF CACHE DUMP -------<");
                    println!("  >> CACHE PATH <<");
                    println!("    {:?}", cli.cache.canonicalize());

                    println!("\n\n  >> PEOPLE ({}) <<", cache.people.len());
                    for p in cache.people.values() {
                        println!("    {} - {} {}", p.sciper, p.firstname, p.name);
                    }
                    println!("\n\n  >> UNITS ({}) <<", cache.units.len());
                    for u in cache.units.values() {
                        let number_of_people = match u.clone().people {
                            Some(v) => v.len().to_string(),
                            None => "?".to_string()
                        };
                        println!("    {} ({}) - {}", u.acronym, number_of_people, u.name);
                    }
                    println!(">------- END OF CACHE DUMP   -------<");
                },

                Err(err) => {
                    LookupError::CacheError(err).print();
                    exit(1);
                }
            }
        }
    }
}
